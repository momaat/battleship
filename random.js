function randomShips(size, amountOfShips){
    let totalSize = size * size;
    let randomArr = [];
    for (let i = 0; i < amountOfShips * 2; i++) {
        let value = Math.ceil(Math.random() * Math.floor(totalSize));
        if (value !== 0){
            randomArr.push(value);
        }
    }
    let uniqueArr = [...new Set(randomArr)];
    randomArr = [];
    for (let j = 0; j < amountOfShips; j++) {
        randomArr.push(uniqueArr[j]);
    }
    randomArr.sort((a, b) => a - b);
    return randomArr;
}

let randomBombsArray = [];

function randomBombs(shipsArray, gridSize, maxClusterBombs, maxAtomBombs) {
    let totalGridSize = gridSize * gridSize;
    if (gridSize == 6){
        maxClusterBombs = 1;
        maxAtomBombs = 1;
    } else if (gridSize == 10){
        maxClusterBombs = 2;
        maxAtomBombs = 3;
    } else if (gridSize == 15){
        maxClusterBombs = 3;
        maxAtomBombs = 5;
    }
    //console.log('maxClusters:',maxClusterBombs);
    //console.log('maxAtoms:',maxAtomBombs);
    maxBombs = maxClusterBombs + maxAtomBombs; 
    //console.log('shipsArray inside randomClusterBombs:',shipsArray);
    //console.log('maxclusterBombs:',maxClusterBombs);
    
    for (let i = 1; i <= maxBombs;) {
        let bombLocations = Math.floor(Math.random() * totalGridSize);

        if (bombLocations !== 0 && (!(shipsArray.includes(parseInt(bombLocations)))) && (!(randomBombsArray.includes(bombLocations))))  {
            randomBombsArray.push(bombLocations);
            i++;
        }
    }
    randomBombsArray.push(maxClusterBombs);
    console.log('bombLocations:',randomBombsArray);
}

// let bombArray = [];

// function randomClusterBombs() {
    
//     for (let i = 1; i <= maxClusterBombs;) {
//         let clusterBombLocations = Math.floor(Math.random() * gridSize * gridSize);

//         if (clusterBombLocations !== 0 && (!(shipsArray.includes(parseInt(clusterBombLocations)))) && (!(bombArray.includes(parseInt(clusterBombLocations)))))  {
//             bombArray.push(clusterBombLocations);
//             i++
//         }
//     }
//     console.log('bombLocations:',bombArray);
// }