Battleship

This project was a four person's group work in the Full Stack -training organized by University of Turku in 03-09/2020.
~~The game can be found at https://mybattleship2020.herokuapp.com/.~~ (Due to changes in Heroku's services, this game is no longer available in Heroku. Please see the img-folder to watch an mp4-video of the game.)


Game instructions

There are three different size of gameboard to choose from: 6x6, 10x10 and 15x15. Depending on the size the program randomly places 5, 10 or 15 ships on the board to be found. The size of all the ships is one cell.

The game proceeds by clicking one cell at a time to discover the ships, and ends when all the ships have been found. A popup will emerge to inform the player that he/she has won and displays the scores.

Each ship adds 10 points to the player. If two or more ships are found consecutively the player gains 10 extra points per ship. Every miss click on the other hand subtracts one point.  

In the grid there are also special bombs hidden: a cluster bomb (c) and an atom bomb (a). Depending on the size of the gameboard the number of the bombs are 1c+1a, 2c+3a and 3c+5a.  

After finding a cluster bomb, the player can use it at a preferable time by pressing the cluster bomb button below the gameboard. The cluster bomb then blows up 10 randomly chosen cells. 
The atom bomb becomes active immediately after it has been found and when the player clicks a preferable cell, a 3x3 cell area is blown up.

The browser console displays the coordinates for the ships and the bombs. The player can activate godmode and get access to an unlimited number of atom bombs by typing "godmode = 1" in the browser console and pressing enter. 

![Image of the game](img/screenshot_battleship.png "Battleship")
