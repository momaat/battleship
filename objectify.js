function getBigBombEffectCoordinates(target, gridSizeMultiplier, bombSizeMultiplier) {
    //function td.clickable onclick
    //e.target.id => do something

    //e target.id:
    let targetID = parseInt(target);

    //3x3 (1) or 5x5 (2):
    let BSM = bombSizeMultiplier;
    // gridSize:
    let size = gridSizeMultiplier;

    // construct grid object array:
    let idNumber = 1;
    let idCoordinatesArray = [];
    for (let i = 1; i <= size; i++) {
        for (let j = 1; j <= size; j++) {
            idCoordinatesArray.push({ id: idNumber, x: j, y: i });
            idNumber++;
        }
    }
    console.log(idCoordinatesArray);

    //construct regular and irregular id arrays for varying grid placement conditions onclick: 

    //targetID == e.target.id
    let currentAreaID = idCoordinatesArray.find(item => item.id == targetID);
    console.log(`ID: ${currentAreaID.id}, x: ${currentAreaID.x}, y: ${currentAreaID.y}`);

    if (currentAreaID.x == 1 && currentAreaID.y == 1) {
        console.log('TopLeftClick');
    }
    else if (currentAreaID.y == 1 && currentAreaID.x != 1 && currentAreaID.x != size) {
        console.log('TopClick');
    }
    else if (currentAreaID.x == size && currentAreaID.y == 1) {
        console.log('TopRightClick');
    }
    else if (currentAreaID.x == size && currentAreaID.y != 1 && currentAreaID.y != size) {
        console.log('RightClick');
    }
    else if (currentAreaID.x == size && currentAreaID.y == size) {
        console.log('BottomRightClick');
    }
    else if (currentAreaID.y == size && currentAreaID.x != 1 && currentAreaID.x != size) {
        console.log('BottomClick');
    }
    else if (currentAreaID.x == 1 && currentAreaID.y == size) {
        console.log('BottomLeftClick');
    }
    else if (currentAreaID.x == 1 && currentAreaID.y != 1 && currentAreaID.y != size) {
        console.log('LeftClick');
    }
    else if (currentAreaID.x > 1 && currentAreaID.x < size && currentAreaID.y > 1 && currentAreaID.y < size) {
        console.log('CenterClick');
    } else {
        return;
    }
    if (currentAreaID.x > 0 && currentAreaID.x < size+1 && currentAreaID.y > 0 && currentAreaID.y < size+1){
        let areaOfEffect = [];
        areaOfEffect.push({ id: targetID, x: currentAreaID.x, y: currentAreaID.y });
        for (let i = 1; i <= BSM; i++) {
            areaOfEffect.push({ id: targetID - (size * i) - i, x: currentAreaID.x - i, y: currentAreaID.y - i });
            areaOfEffect.push({ id: targetID - (size * i), x: currentAreaID.x, y: currentAreaID.y - i });
            areaOfEffect.push({ id: targetID - (size * i) + i, x: currentAreaID.x + i, y: currentAreaID.y - i });
            areaOfEffect.push({ id: targetID + i, x: currentAreaID.x + i, y: currentAreaID.y });
            areaOfEffect.push({ id: targetID + (size * i) + i, x: currentAreaID.x + i, y: currentAreaID.y + i });
            areaOfEffect.push({ id: targetID + (size * i), x: currentAreaID.x, y: currentAreaID.y + i });
            areaOfEffect.push({ id: targetID + (size * i) - i, x: currentAreaID.x - i, y: currentAreaID.y + i });
            areaOfEffect.push({ id: targetID - i, x: currentAreaID.x - i, y: currentAreaID.y });
            if (i == 2) {
                areaOfEffect.push({ id: targetID - (size * i) - 1, x: currentAreaID.x - 1, y: currentAreaID.y - i });
                areaOfEffect.push({ id: targetID - (size * i) + 1, x: currentAreaID.x + 1, y: currentAreaID.y - i });
                areaOfEffect.push({ id: targetID - size + i, x: currentAreaID.x + i, y: currentAreaID.y - 1 });
                areaOfEffect.push({ id: targetID + size + i, x: currentAreaID.x + i, y: currentAreaID.y + 1 });
                areaOfEffect.push({ id: targetID + (size * i) + 1, x: currentAreaID.x + 1, y: currentAreaID.y + i });
                areaOfEffect.push({ id: targetID + (size * i) - 1, x: currentAreaID.x - 1, y: currentAreaID.y + i });
                areaOfEffect.push({ id: targetID + size - i, x: currentAreaID.x - i, y: currentAreaID.y + 1 });
                areaOfEffect.push({ id: targetID - size - i, x: currentAreaID.x - i, y: currentAreaID.y - 1 });
            }
        }
        areaOfEffect.sort((a, b) => a.id - b.id);
        let filteredAreaOfEffect = areaOfEffect.filter(item => item.x > 0 && item.x <= size && item.y > 0 && item.y <= size);
        filteredAreaOfEffect.sort((a, b) => a.id - b.id);
        areaOfEffect = filteredAreaOfEffect;
        //console.log(areaOfEffect);

        return areaOfEffect;
    }
}