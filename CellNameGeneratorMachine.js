// Original program by Sebastian Sid. Creative Commons copyright applies.
function makeArr(range){
let gridSize = range;
let letters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ'; // 26
let letters2 = letters.substring(0, gridSize);
let lettersArr = letters2.split('');

if (gridSize > letters.length){
    let requiredLength = (Math.ceil((gridSize / letters.length)*100)).toFixed(3)/100;
    let newLetterAmount = (Math.ceil(letters.length * requiredLength)) - letters.length;
    let letterAppendString = letters.substring(0, newLetterAmount);
    let setupDoubleLetterArray = letterAppendString.split('');
    for (let l = 0; l < letterAppendString.length; l++){
        let letterAmount = `${setupDoubleLetterArray[l]}`;
        for (let letterAmountMultiplier = 1; letterAmountMultiplier <= Math.floor(requiredLength); letterAmountMultiplier++){
            letterAmount += `${setupDoubleLetterArray[l]}`;
        }
        lettersArr.push(letterAmount);
    }
}

var numbersArr = [];
for (let i = 1; i <= gridSize; i++){
    numbersArr.push(i);
}
let gridCellNameArr = [];
for (let j = 0; j < gridSize; j++){
    for (let k = 0; k < gridSize; k++){
        gridCellNameArr.push(`${lettersArr[j]}${numbersArr[k]}`);
    }
}
return gridCellNameArr;
}